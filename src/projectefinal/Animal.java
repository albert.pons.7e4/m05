package projectefinal;

abstract class Animal {
    int x, y;
    boolean mort = false;

    public abstract String toString();

    public void mor() {
        this.mort = true;
    }

    public abstract void mou();

    public Animal(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
