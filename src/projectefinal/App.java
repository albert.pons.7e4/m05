package projectefinal;//versió 0.1

import java.util.ArrayList;
import java.util.Scanner;

/**
 * The type App.
 */
public class App {
    /**
     * The constant CONILLS.
     */
    public static final int CONILLS = 1, /**
     * The Llops.
     */
    LLOPS = 10, /**
     * The Columnes.
     */
    COLUMNES = 16, /**
     * The Files.
     */
    FILES = 16, /**
     * The Roques.
     */
    ROQUES = 10, /**
     * The Aigua.
     */
    AIGUA = 15;
    /**
     * The constant animals.
     */
    public static ArrayList<Animal> animals = new ArrayList<>();
    /**
     * The constant tauler.
     */
    public static Bloc[][] tauler = new Bloc[COLUMNES][FILES];
    private int totalAnimals;
    private int torns = 0;
    private String hora = "\uD83C\uDF23";

    /**
     * Inicialitza.
     */
    public void inicialitza() {
        for (int i = 0; i < COLUMNES; i++) {
            for (int j = 0; j < FILES; j++) {
                int terreny = (int) (Math.random() * 100);
                int t = 0;
                int animal = (int) (Math.random() * 100);
                if (terreny < ROQUES) t = 1;
                else if (terreny < ROQUES + AIGUA) t = 2;
                Animal a;
                if (t == 2 || animal >= LLOPS + CONILLS) {
                    tauler[i][j] = new Bloc(t);
                } else {
                    if (animal < LLOPS) {
                        a = new Llop(i, j);
                    } else {
                        a = new Conill(i, j);
                    }
                    animals.add(a);
                    tauler[i][j] = new Bloc(t, a);
                }
            }
        }
        totalAnimals = animals.size();
    }

    /**
     * Mostra.
     */
    public void mostra() {
        for (int i = 0; i < FILES; i++) {
            for (int j = 0; j < COLUMNES; j++) {
                System.out.print(tauler[i][j]);
            }
            System.out.println();
        }
        if (torns % 5 == 0) {
            hora = "\uD83C\uDF23";
        }
        if (torns % 10 == 0) {
            hora = "\u263E";
        }
        System.out.println("\uD83E\uDDE1" + animals.size() + "/" + totalAnimals + "  \uD83D\uDD50" + torns + " " + hora);
    }

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {

        App joc = new App();
        joc.inicialitza();
        Scanner scanner = new Scanner(System.in);
        while (animals.size() > 0) {
            ArrayList<Animal> morts = new ArrayList<>();
            for (Animal animal : animals) {
                animal.mou();
            }
            for (Animal animal : animals) {
                if (animal.mort) morts.add(animal);
            }
            for (Animal animal : morts) {
                animals.remove(animal);
            }
            joc.torns++;
            joc.mostra();
            scanner.nextLine();
        }
        joc.mostra();
        System.out.println("Has destruit tota vida a la vista, bon treball \uD83D\uDE05");
    }
}

