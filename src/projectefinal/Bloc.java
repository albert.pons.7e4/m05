package projectefinal;

import java.util.Objects;

class Bloc {
    private final int terreny; // 0(terra, verd) 1(roca, gris) 2(aigua, blau)
    private Animal animal = null;

    public String toString() {
        // terra per defecte
        String grafic;
        switch (terreny) {
            case 1:
                grafic = "\033[0;100m";
                break;
            case 2:
                grafic = "\u001B[44m";
                break;
            default:
                grafic = "\u001B[42m";
                break;
        }
        // 2 espais per a mantindre l'alineació
        return grafic + Objects.requireNonNullElse(animal, "  ") + "\033[0m"; // reset, torna al color per defecte

    }

    public Bloc(int terreny) {
        this.terreny = terreny;
    }

    public Bloc(int terreny, Animal animal) {
        this.animal = animal;
        this.terreny = terreny; // this(terreny); no era així per a reutilitzar un constructor sobrecarregat? :S
    }

    public boolean esAigua() {
        return terreny != 2;
    }

    /**
     * @return Aquest metode retorna true si el terreny és roca i fals si no
     */
    public boolean esRoca() {
        return terreny == 1;
    }

    public boolean esAnimal() {
        return animal != null;
    }

    public boolean esConill() {
        return animal instanceof Conill;
    }

    public boolean esLlop() {
        return animal instanceof Llop;
    }

    public Animal getAnimal() {
        return this.animal;
    }

    public void delAnimal() {
        this.animal = null;
    }

    /**
     * @param animal És l'objecte animal que volem col.locar
     */
    public void setAnimal(Animal animal) {
        this.animal = animal;
    }
}
