package projectefinal;

class Conill extends Animal {
    @Override
    public String toString() {
        return "\uD83D\uDC30";
    } // icona de conill

    public Conill(int x, int y) {
        super(x, y);
    } // reutilitzem el constructor de la classe mare

    public void mou() {
        if (mort) return;
        Bloc[][] tauler = App.tauler;
        int i = (int) (Math.random() * 3) - 1 + x;
        int j = (int) (Math.random() * 3) - 1 + y;
        if (i >= 0 && i < tauler.length && j >= 0 && j < tauler[0].length && tauler[i][j].esAigua() && !tauler[i][j].esAnimal()) {
            tauler[x][y].delAnimal();
            tauler[i][j].setAnimal(this);
            x = i;
            y = j;
        }
    }
}
